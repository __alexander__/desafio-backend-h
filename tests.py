import unittest
from pokeapi import PokemonSpecie, PokemonType


class TestPokemonSpecie(unittest.TestCase):
    def setUp(self):
        self.all_pokemon = PokemonSpecie.list()

    def test_list(self):
        total_pokemon = 898
        self.assertGreaterEqual(len(self.all_pokemon), total_pokemon)

    def test_get(self):
        pokemon_name = "psyduck"
        pokemon_generation = "generation-i"
        pokemon = PokemonSpecie.get(pokemon_name)
        self.assertEqual(pokemon["name"], pokemon_name)
        self.assertEqual(pokemon["generation"]["name"], pokemon_generation)


class TestPokemonType(unittest.TestCase):
    def test_get(self):
        type_name = "dragon"
        pokemon = "rayquaza"
        another_pokemon = "dratini"
        all_pokemon = PokemonType.get(type_name)["pokemon"]
        all_pokemon_names = [pokemon["pokemon"]["name"] for pokemon in all_pokemon]
        self.assertIn(pokemon, all_pokemon_names)
        self.assertIn(another_pokemon, all_pokemon_names)


if __name__ == "__main__":
    unittest.main()
