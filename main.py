from collections import Counter
import requests
from pokeapi import PokemonSpecie, PokemonType
from utils import extract_id_from_url


def question_01():
    all_pokemon = PokemonSpecie.list()
    valid_pokemon = []
    for pokemon in all_pokemon:
        if "at" in pokemon["name"] and Counter(pokemon["name"])["a"] == 2:
            valid_pokemon.append(pokemon["name"])

    # for pokemon in valid_pokemon:
    #     print(pokemon)
    print("Pregunta #1:", len(valid_pokemon))
    return len(valid_pokemon)


def question_02():
    specie_name = "raichu"
    specie = PokemonSpecie.get(name=specie_name)
    egg_groups = specie.get("egg_groups")
    compatible_pokemon = []
    for egg_group in egg_groups:
        egg_group_pokemon = requests.get(egg_group["url"]).json()["pokemon_species"]
        compatible_pokemon = compatible_pokemon + [
            pokemon["name"] for pokemon in egg_group_pokemon
        ]

    # for pokemon in set(compatible_pokemon):
    #     print(pokemon)
    print("Pregunta #2:", len(set(compatible_pokemon)))
    return len(set(compatible_pokemon))


def question_03():
    name = "fighting"
    max_id = 151
    type_fighting = PokemonType.get(name)
    fighting_pokemon = type_fighting["pokemon"]
    results = []
    for pokemon in fighting_pokemon:
        url = pokemon["pokemon"]["url"]
        if int(extract_id_from_url(url)) <= max_id:
            pokemon_detail = requests.get(url).json()
            results.append(pokemon_detail["weight"])

    print(f"Pregunta #3: [{max(results)},{min(results)}]")
    return [max(results), min(results)]


if __name__ == "__main__":
    question_01()
    question_02()
    question_03()
