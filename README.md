# Desafío Técnico H

## Preguntas:

1. Obtén cuantos pokemones poseen en sus nombres “at” y tienen 2 “a” en su nombre, incluyendo la primera del “at”. Tu respuesta debe ser un número.
2. ¿Con cuántas especies de pokémon puede procrear raichu? (2 Pokémon pueden procrear si están dentro del mismo egg group). Tu respuesta debe ser un número. Recuerda eliminar los duplicados.
3. Entrega el máximo y mínimo peso de los pokémon de tipo fighting de primera generación (cuyo id sea menor o igual a 151). Tu respuesta debe ser una lista con el siguiente formato: [1234, 12], en donde 1234 corresponde al máximo peso y 12 al mínimo.

## Install:

    pip install requirements.txt
    python main.py

## Tests:

    python tests.py

## TODO:

1. En caso se utilice de forma repetida las consultas a algunos endpoints como a la lista de todos los pokemons, se debe considerar el almacenamiento de los resultados en caché para agilizar las futuras consultas.

2. Los tests se conectan directamente a PokeAPI, pero se debe evaluar el utilizar "Mocks" por sus ventajas como el acelerar los tests.
