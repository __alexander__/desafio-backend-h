import re


def extract_id_from_url(url):
    regex = "https://pokeapi.co/api/v2/pokemon/(?P<id>[0-9]*)/"
    match = re.search(regex, url)
    if match:
        return match.groups()[0]
