import requests


class PokemonSpecie:
    base_url = "https://pokeapi.co/api/v2/"
    limit = 200

    @classmethod
    def list(cls):
        url = f"{cls.base_url}pokemon-species/?limit={cls.limit}"
        results = []
        while url:
            response = requests.get(url)
            results = results + response.json()["results"]
            url = response.json().get("next")
        return results

    @classmethod
    def get(cls, name):
        url = f"{cls.base_url}pokemon-species/{name}/"
        return requests.get(url).json()


class PokemonType:
    base_url = "https://pokeapi.co/api/v2/"

    @classmethod
    def get(self, name):
        url = f"{self.base_url}type/{name}/"
        return requests.get(url).json()
